capped-collection:
//Why "max" is required in capped collection?
max option is used to specify the limit on maximum number of documents allowed in the collection.

//creating capped collections
db.createCollection("kk", {capped : true, size : 10000, max : 3 })
{ "ok" : 1 } -capped collection created with max of 3 documents


db.kk.insert({"name":"chinna"})
WriteResult({ "nInserted" : 1 })
db.kk.insert({"name":"sai"})
WriteResult({ "nInserted" : 1 })
db.kk.insert({"name":"kumar"})
WriteResult({ "nInserted" : 1 })

3 document inserted in kk collection
db.kk.find()
{ "_id" : ObjectId("5f5cf68282f45540ec930947"), "name" : "chinna" }
{ "_id" : ObjectId("5f5cf69a82f45540ec930948"), "name" : "sai" }
{ "_id" : ObjectId("5f5cf6b682f45540ec930949"), "name" : "kumar" }

trying to insert 4th document
db.kk.insert({"name":"muni"})
WriteResult({ "nInserted" : 1 })

db.kk.find()
{ "_id" : ObjectId("5f5cf69a82f45540ec930948"), "name" : "sai" }
{ "_id" : ObjectId("5f5cf6b682f45540ec930949"), "name" : "kumar" }
{ "_id" : ObjectId("5f5cf8bf82f45540ec93094a"), "name" : "muni" }

firt document removed and inserted with new document with different _id

gridFS:
syntax:
var gs = new mongodb.GridStore(db, filename, mode[, options])

mongofiles -d gridfs put "C:\Users\Jeyapriya\Desktop\pavan.mp4"  --db and collections created 
2020-09-13T00:55:18.952+0530    connected to: mongodb://localhost/
2020-09-13T00:55:22.725+0530    added gridFile: C:\Users\Jeyapriya\Desktop\pavan.mp4

show collections
fs.chunks
fs.files

db.fs.files.find().pretty()
{
        "_id" : ObjectId("5f5d209ea08f2addfd4a9e25"),
        "length" : NumberLong(44767619),
        "chunkSize" : 261120,
        "uploadDate" : ISODate("2020-09-12T19:25:22.724Z"),
        "filename" : "C:\\Users\\Jeyapriya\\Desktop\\pavan.mp4",
        "metadata" : {

        }
}

db.fs.chunks.find().pretty()

